## [Release 0.4.1](https://gitlab.com/mipimipi/armutils/-/tags/0.4.1) (2020-05-11)

### Changed

* `mkarmroot`: Replaced `wget` by `curl` to download ARM image
* `makepkg_sudo`: Change "fakeroot" by "sudo" in messages

### Removed

* `mkarmroot`: Removed mounting chroot folder to itself


## [Release 0.4.0](https://gitlab.com/mipimipi/armutils/-/tags/0.4.0) (2020-05-05)

### Added

* `makepkg_sudo`, a fork of `makepkg` that uses `sudo` instead of `fakeroot`. That's required because of [this problem](https://archlinuxarm.org/forum/viewtopic.php?f=57&t=14466).

## [Release 0.3.0](https://gitlab.com/mipimipi/armutils/-/tags/0.3.0) (2020-05-01)

### Changed

* `arm-nspawn`: Removed binding of /var/cache/pacman/pkg to chroot container.
* `makearmchroot`: Corrected creation of build user

## [Release 0.2.0](https://gitlab.com/mipimipi/armutils/-/tags/0.2.0) (2020-04-26)

### Added

* `arm-nspawn` as counterpart to `arch-nspawn`

## [Release 0.1.1](https://gitlab.com/mipimipi/armutils/-/tags/0.1.1) (2020-04-25)

### Changed

* Adopted latest change of makechrootpkg to makearmpkg
* Corrected Makefile

## [Release 0.1.0](https://gitlab.com/mipimipi/armutils/-/tags/0.1.0) (2020-04-25)

### Added

* Initial version of `mkarmroot`
* Initial version of `makearmpkg`
